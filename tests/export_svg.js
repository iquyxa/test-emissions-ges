function exportSVG(gd) {
/*gd.layout.annotations = [{
  "text": "Champ : FM.<br><em>Source : <a href='https://www.citepa.org/fr/secten/'>Citepa</a></em>.",
  "xref": "paper", "x": 0, "xanchor": "left", "showarrow": false, "align":"left", "font":{"size":14}
}]*/
alert(gd.layout.annotations[0].text);
//Plotly.downloadImage(gd, {format: 'png', width: 1000, height: 500, filename: 'zz'});
Plotly.toImage(gd, {format: 'svg', imageDataOnly: true, 'width': 1200, 'height': 600}).then(function(url) {
  var conteneur = document.createElement('div');
  conteneur.innerHTML = url;
  
  var svg = conteneur.getElementsByTagName('svg')[0];
  var hauteur = parseInt(svg.getAttribute('height'));
  var viewBox = svg.getAttribute('viewBox');
  
  var elements = svg.getElementsByClassName("legendtoggle");
  while (e = elements[0]) {
    e.parentNode.removeChild(e);
  }
  
  var annotations = svg.getElementsByClassName('annotation');
  if (annotations.length > 0) {
    var annotation = annotations[0];
    var hauteur_legende = parseInt(annotation.getElementsByTagName('rect')[0].getAttribute('height'));
    var g_legende = svg.getElementsByClassName('cursor-pointer')[0];
    
    svg.setAttribute('height', hauteur + hauteur_legende);
    svg.getElementsByTagName('rect')[0].setAttribute('height', hauteur + hauteur_legende);
    viewBox = viewBox.split(" ")
    viewBox[3] = hauteur + hauteur_legende;
    viewBox = viewBox.join(' ');
    svg.setAttribute('viewBox', viewBox);
  
    var transform = g_legende.getAttribute('transform');
    transform = transform.split(",")
    transform = transform[0] + ",600)"
    g_legende.setAttribute('transform', transform);
    
    var lien = annotation.getElementsByTagName('a');
    if (lien.length > 0) {
      lien[0].parentNode.setAttribute('style', 'font-style:italic;')
    }
  }
  
  var a = document.createElement('a');
  const data_uri = 'data:image/svg+xml,' + encodeURIComponent(conteneur.innerHTML);
  a.href = data_uri;
  a.download = 'graphique.svg';
  document.body.appendChild(a);
  a.click();
  URL.revokeObjectURL(data_uri);
})
}