import cdstoolbox as ct
import datetime
import calendar

def temp_quot(annee, mois, jours):
    era5 = ct.catalogue.retrieve(
        'reanalysis-era5-single-levels',
        {
            'product_type': 'reanalysis',
            'variable': '2m_temperature',
            'year': str(annee),
            'month': str(mois),
            'day': ["%02d" % i for i in jours],
            'time': [f"{i:02d}:00" for i in range(24)],
        },
    )
    
    # Agrégation spatiale
    temp = ct.geo.spatial_average(era5)
    return temp

@ct.application()
@ct.output.download()
def download(debut, fin = ""):
    start = datetime.datetime.strptime(debut, "%Y-%m-%d").date()
    if fin == "":
        end = datetime.date.today()
    else:
        end = datetime.datetime.strptime(fin, "%Y-%m-%d").date()
        
    series = []
    while start <= end:
        annee = start.year
        mois = start.month
        jour_max = calendar.monthrange(annee, mois)[1]
        if annee == end.year and mois == end.month:
            jour_max = end.day
        
        tq = temp_quot(annee, mois, range(start.day, jour_max + 1))
        series.append(tq)
        mois = mois % 12 + 1
        if mois == 1:
            annee += 1
        start = datetime.date(annee, mois, 1)
    
    
    temp_hourly = ct.cube.concat(series, dim = "time")
    nb_heures = ct.cdm.get_coordinates(temp_hourly)["time"]["data"]
    
    last = nb_heures[-1]
    if last.hour < 23:
        stop = str(last.date() - datetime.timedelta(days = 1))
        temp = ct.cube.select(temp_hourly, stop_time = stop)
    else:
        temp = temp_hourly
    
    # Moyenne quotidienne
    temp_daily = ct.climate.daily_mean(temp)

    return(temp_daily)
