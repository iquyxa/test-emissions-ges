# Test de visualisation des émissions françaises de gaz à effet de serre

https://emissions.gth.ovh/

## Sources
- Inventaire d'émissions au format Secten : [Citepa](https://www.citepa.org/donnees-air-climat/donnees-gaz-a-effet-de-serre/secten/)
- Empreinte carbone : [Sdes](https://www.statistiques.developpement-durable.gouv.fr/emissions-de-gaz-effet-de-serre-et-empreinte-carbone-de-la-france-une-baisse-significative-en-2023?dossier=1286)
- Inventaire d'émissions au format CCNUCC : [Citepa](https://www.citepa.org/donnees-air-climat/donnees-gaz-a-effet-de-serre/nid-dans-le-cadre-de-la-ccnucc/)
- Comptes d’émissions atmosphériques Namea : [Eurostat](https://ec.europa.eu/eurostat/databrowser/view/env_ac_ainah_r2/default/table)

## Accès direct aux différents onglets

### Secten

(En gras la valeur par défaut si le paramètre n'est pas renseigné)

> /#secten?onglet=Graphique&format=niveau&reference=1990&presentation=Secteurs agrégés&secteurs=a;b;c&transports-internationaux=0&champ=FR&gaz=GES

- `onglet` = **Graphique** | Tableau | Données
- `format` = **niveau** | indice | repartition
- `reference` = NNNN (année entre **1990** et 2022 correspond à la référence 100 en format Indice)
- `presentation` = **Secteurs agrégés** | Transports | ... | Secteurs détaillés (correspond à la liste de choix "Présentation")
- `secteurs` = Chimie;Deux roues;Bovins (Liste des secteurs séparés par un ; à afficher lorsque presentation=Secteurs détaillés)
- `transports-internationaux` = **0** | 1 (Prendre en compte ou non les transports internationaux lorsque presentation=Secteurs agrégés ou Transports)
- `champ` = **FR** | FM
- `gaz` = **GES** | CO2 | CH4 | N2O | GF | HFC | PFC | SF6 | NF3

### Empreinte carbone

> /#empreinte?onglet=Graphique&presentation=origine&format=niveau&reference=1995&emissions-individuelles=1

- `onglet` = **Graphique** | Tableau | Données
- `presentation` = **origine** | gaz | postes
- `format` = **niveau** | indice
- `reference` = NNNN (année entre **1990** et 2023 correspond à la référence 100 en format Indice)
- `emissions-individuelles` = **1** | 0 (Afficher ou non les émissions par personne)

### Panorama

> /#panorama

### CCNUCC

(En gras la valeur par défaut si le paramètre n'est pas renseigné)

> /#ccnucc?onglet=Graphique&format=niveau&reference=1990&presentation=Secteurs agrégés&secteurs=a;b;c&transports-internationaux=0&champ=FR&gaz=GES

- `onglet` = **Graphique** | Tableau | Données
- `format` = **niveau** | indice | repartition
- `reference` = NNNN (année entre **1990** et 2021 correspond à la référence 100 en format Indice)
- `presentation` = **Secteurs agrégés** | Agriculture | ... | Secteurs détaillés (correspond à la liste de choix "Présentation")
- `secteurs` = Branche énergie;Industrie des métaux;Forêts (Liste des secteurs séparés par un ; à afficher lorsque presentation=Secteurs détaillés)
- `transports-internationaux` = **0** | 1 (Prendre en compte ou non les transports internationaux lorsque presentation=Secteurs agrégés ou Transports)
- `champ` = **FR** | FR_TOM
- `gaz` = **GES** | CO2 | CH4 | N2O | HFC | PFC | SF6 | NF3

### Namea

(En gras la valeur par défaut si le paramètre n'est pas renseigné)

> /#namea?onglet=Graphique&format=niveau&reference=2008&presentation=Secteurs agrégés&secteurs=a;b;c&gaz=GES

- `onglet` = **Graphique** | Tableau | Données
- `format` = **niveau** | indice | repartition
- `reference` = NNNN (année entre **2008** et 2021 correspond à la référence 100 en format Indice)
- `presentation` = **Secteurs agrégés** | Industrie-Construction | ... | Secteurs détaillés (correspond à la liste de choix "Présentation")
- `secteurs` = Commerce;Transports - ménages;Industries extractives (Liste des secteurs séparés par un ; à afficher lorsque presentation=Secteurs détaillés)
- `gaz` = **GES** | CO2 | CH4 | N2O | HFC | PFC | NF3_SF6

### Température

> /#temperature?geo=Monde&periodicite=A&glissement=10&format=graphique&mois=0&variable=Température&cumul=0&ga=0

- `geo` = **Monde** | France métropolitaine
- `periodicite` = **A** (Annuelle) | M (Mensuelle) | J (Quotidienne)
- `glissement` = NN (nombre d'années de glissement annuel compris entre 1 et 40 ; par défaut **10** ans ; uniquement pour graphiques annuels et mensuels)
- `format` = **graphique** | heatmap
- `mois` = **0** (Tous les mois) | 1 (Janvier) | ... | 12 (Décembre) (Filtre éventuel sur un mois ; uniquement pour les graphiques mensuels)
- `variable` = **Température** | Anomalie (uniquement pour les graphiques quotidiens)
- `cumul` = **0** | 1 (Représenter le cumul annuel à date lorsque periodicite=M ou J)
- `ga` = **0** | 1 (Représenter le glissement annuel lorsque periodicite=J)

### Divers

> /#divers

