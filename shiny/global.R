# Chargement des données et modules ----

cache <- cache_mem(max_size = 20e6, evict = "lru")
shinyOptions(cache = cache)
# options(shiny.autoreload = FALSE)
cache_disque <- cache_disk(dir = file.path(Sys.getenv("cache_disque"), "emissions-ges"), max_size = 100e6, evict = "lru")

## Présentation ----
# Liste des onglets
ONGLETS <- data.frame(
  nom = c("Inventaire Secten", "Empreinte carbone", "Panorama", "Inventaire CCNUCC", "Namea", "Température", "Divers"),
  url = c("secten", "empreinte", "panorama", "ccnucc", "namea", "temperature", "divers"),
  creation = rep(FALSE, 7),
  ui = c("emissions_ui", "empreinte_ui", "panorama_ui", "emissions_ui", "emissions_ui", "temperature_ui", "divers_ui"),
  server = c("emissions_server", "empreinte_server", "panorama_server", "emissions_server", "emissions_server", "temperature_server", "divers_server")
)

# Correspondance formule chimique -> nom français des GES
liste_ges <- c("Total gaz à effet de serre" = "GES",
               "Dioxyde de carbone (CO₂)" = "CO2",
               "Méthane (CH₄)" = "CH4",
               "Protoxyde d'azote (N₂O)" = "N2O",
               "Total gaz fluorés (HFC, PFC, NF₃, SF₆)" = "GF",
               "Hydrofluorocarbures (HFC)" = "HFC",
               "Perfluorocarbures (PFC)" = "PFC",
               "Hexafluorure de soufre (SF₆) et Trifluorure d'azote (NF₃)" = "NF3_SF6",
               "Hexafluorure de soufre (SF₆)" = "SF6",
               "Trifluorure d'azote (NF₃)" = "NF3")

## Données sur les émissions ----
emissions_secten <- read_parquet("data/emissions_france/secten.parquet")
emissions_ccnucc <- read_parquet("data/emissions_france/ccnucc.parquet")
emissions_namea <- read_parquet("data/emissions_france/namea.parquet")
empreinte <- read_parquet("data/emissions_france/empreinte.parquet")

emissions_fr <- list(
  "secten" = list(
    df = emissions_secten,
    annee_min = as.integer(min(names(emissions_secten))),
    annee_max = as.integer(gsub("[^0-9]+", "", names(emissions_secten %>% select(last_col())))),
    presentation = c("Secteurs agrégés",
                     emissions_secten %>% filter(niveau == 1) %>% pull(parent) %>% unique(),
                     "Secteurs détaillés"),
    secteurs_detailles = unique(emissions_secten$secteur),
    champ = unique(emissions_secten$champ),
    gaz = liste_ges[liste_ges %in% unique(emissions_secten$gaz)],
    source = "Citepa, inventaire format Secten, juin 2024",
    url = "https://www.citepa.org/donnees-air-climat/donnees-gaz-a-effet-de-serre/secten/",
    titre = "Émissions territoriales au format Secten"
  ),
  "ccnucc" = list(
    df = emissions_ccnucc,
    annee_min = as.integer(min(names(emissions_ccnucc))),
    annee_max = as.integer(names(emissions_ccnucc %>% select(last_col()))),
    presentation = c("Secteurs agrégés",
                     emissions_ccnucc %>% filter((niveau == 1 & secteur != "CO2 indirect dû à l'oxydation d'autres gaz") | (parent == "Energie" & niveau == 2)) %>% pull(secteur) %>% unique(),
                     "Secteurs détaillés"),
    secteurs_detailles = unique(emissions_ccnucc$secteur),
    champ = unique(emissions_ccnucc$champ),
    gaz = liste_ges[liste_ges %in% c(unique(emissions_ccnucc$gaz))],
    source = "Citepa, inventaire format CCNUCC, juin 2024",
    url = "https://www.citepa.org/donnees-air-climat/donnees-gaz-a-effet-de-serre/nid-dans-le-cadre-de-la-ccnucc/",
    titre = "Émissions territoriales au format CCNUCC"
  ),
  "namea" = list(
    df = emissions_namea,
    annee_min = as.integer(min(names(emissions_namea))),
    annee_max = as.integer(names(emissions_namea %>% select(last_col()))),
    presentation = c("Secteurs agrégés",
                     emissions_namea %>% filter(niveau >= 3 & parent != "") %>% pull(parent) %>% unique(),
                     "Secteurs détaillés"),
    secteurs_detailles = unique(emissions_namea$secteur),
    champ = unique(emissions_namea$champ),
    gaz = liste_ges[liste_ges %in% c(unique(emissions_namea$gaz))],
    source = "Citepa, comptes d’émissions atmosphériques Namea",
    url = "https://ec.europa.eu/eurostat/databrowser/view/env_ac_ainah_r2/default/table",
    titre = "Comptes d’émissions atmosphériques Namea"
  )
)

## Modules shiny ----
source("R/utils.R", encoding = "utf-8")
source("modules/emissions_fr.R", encoding = "utf-8")
source("modules/empreinte.R", encoding = "utf-8")
source("modules/temperature.R", encoding = "utf-8")
source("modules/panorama.R", encoding = "utf-8")
source("modules/divers.R", encoding = "utf-8")

# datatable : libellés en français ---
dt_fr = list(
  emptyTable =     "Aucune donnée disponible dans le tableau",
  info =           "Affichage des lignes _START_ à _END_ sur _TOTAL_",
  infoEmpty =      "Affichage de l'élément 0 à 0 sur 0 élément",
  infoFiltered =   "(filtré à partir de _MAX_ éléments au total)",
  infoPostFix =    "",
  infoThousands =  ",",
  lengthMenu =     "Afficher _MENU_ éléments",
  loadingRecords = "Chargement...",
  processing =     "Traitement...",
  search =         "Rechercher :",
  zeroRecords =    "Aucun élément correspondant trouvé",
  paginate = list(
    first =    "Premier",
    last =     "Dernier",
    `next` =     "Suivant",
    previous = "Précédent"
  ),
  aria = list(
    sortAscending   = "activer pour trier la colonne par ordre croissant",
    sortDescending  = "activer pour trier la colonne par ordre décroissant"
  )
)

## Informations sur les données de températures ----
DATA_TEMP <- data.frame(
  periodicite = rep(c("A", "M", "J"), 2),
  geo = c(rep("Monde", 3), rep("France métropolitaine", 3)),
  parquet = c("temp_an_igcc", "temp_mens_hadcrut5", "temp_quot_era5", "temp_an_fr", "temp_mens_fr", "temp_quot_fr"),
  source = c("<a href='https://climatechangetracker.org/igcc'>IGCC, Climate Change Tracker</a>",
             "<a href='https://www.metoffice.gov.uk/hadobs/hadcrut5/'>HadCRUT5, Met Office Hadley Centre</a>",
             "<a href='https://cds.climate.copernicus.eu/datasets/derived-era5-single-levels-daily-statistics?tab=overview'>ERA5, ECMWF, Copernicus Climate Change Service</a>",
             "<a href='https://esd.copernicus.org/articles/13/1397/2022/'>Ribes et al.</a> jusqu'en 2020 ; <a href='https://www.infoclimat.fr/climato/indicateur_national.php'>Infoclimat</a> à partir de 2021",
             "<a href='https://esd.copernicus.org/articles/13/1397/2022/'>Ribes et al.</a> jusqu'en 2021 ; <a href='https://www.infoclimat.fr/climato/indicateur_national.php'>Infoclimat</a> à partir de 2022",
             "<a href='https://esd.copernicus.org/articles/13/1397/2022/'>Ribes et al.</a> jusqu'en 2021 ; <a href='https://www.infoclimat.fr/climato/indicateur_national.php'>Infoclimat</a> à partir de 2022")
)

LISTE_MOIS <- c(
  "Tous" = 0,
  "Janvier" = 1,
  "Février" = 2,
  "Mars" = 3,
  "Avril" = 4,
  "Mai" = 5,
  "Juin" = 6,
  "Juillet" = 7,
  "Août" = 8,
  "Septembre" = 9,
  "Octobre" = 10,
  "Novembre" = 11,
  "Décembre" = 12,
  "DJF" = 120102,
  "MAM" = 030405,
  "JJA" = 060708,
  "SON" = 091011
)

last_URL <- ""

territoires_GCB <- c("Monde",
                     "Europe", "Allemagne", "France métropolitaine", "Royaume-Uni", "Russie", "Ukraine",
                     "Asie", "Chine", "Inde", "Japon",
                     "Amérique", "Canada", "États-Unis",
                     "Afrique", "Océanie", "Transports internationaux")

corresp_continent <- data.frame(
  territoire = territoires_GCB[-1],
  parent = c("", "Europe", "Europe", "Europe", "Europe", "Europe",
             "", "Asie", "Asie", "Asie",
             "", "Amérique", "Amérique",
             "", "", "")
)

baisse_2050 <- data.frame(annee = rep(c(2030, 2035, 2040, 2050), 2),
                          cible = c(rep(1.5, 4), rep(2, 4)),
                          baisse = c(48, 65, 80, 99, 22, 37, 51, 73))

## Graphiques plotly ----
# Palette de couleurs utilisée pour éviter les erreurs "Warning in RColorBrewer ..."
# https://github.com/plotly/plotly.js/blob/v2.14.0/src/components/color/attributes.js
PALETTE <- c("#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf")

# https://showyourstripes.info/
# https://fr.wikipedia.org/wiki/Bandes_du_r%C3%A9chauffement_climatique
WARMING_STRIPES <- colorRamp(c("#08306b", "#08519c", "#2171b5", "#4292c6", "#6baed6", "#9ecae1", "#c6dbef", "#deebf7",
                               "white",
                               "#fee0d2", "#fcbba1", "#fc9272", "#fb6a4a", "#ef3b2c", "#cb181d", "#a50f15", "#67000d"))

# Ajout boutons sauvegarder en PNG, SVG et en HTML
# https://fontawesome.com/icons/camera
PATH_PNG <- "M149.1 64.8L138.7 96H64C28.7 96 0 124.7 0 160V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V160c0-35.3-28.7-64-64-64H373.3L362.9 64.8C356.4 45.2 338.1 32 317.4 32H194.6c-20.7 0-39 13.2-45.5 32.8zM256 192a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"

# https://fontawesome.com/icons/image
PATH_SVG <- "M0 96C0 60.7 28.7 32 64 32H448c35.3 0 64 28.7 64 64V416c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V96zM323.8 202.5c-4.5-6.6-11.9-10.5-19.8-10.5s-15.4 3.9-19.8 10.5l-87 127.6L170.7 297c-4.6-5.7-11.5-9-18.7-9s-14.2 3.3-18.7 9l-64 80c-5.8 7.2-6.9 17.1-2.9 25.4s12.4 13.6 21.6 13.6h96 32H424c8.9 0 17.1-4.9 21.2-12.8s3.6-17.4-1.4-24.7l-120-176zM112 192a48 48 0 1 0 0-96 48 48 0 1 0 0 96z"

# https://fontawesome.com/icons/html5
PATH_HTML <- "M0 32l34.9 395.8L191.5 480l157.6-52.2L384 32H0zm308.2 127.9H124.4l4.1 49.4h175.6l-13.6 148.4-97.9 27v.3h-1.1l-98.7-27.3-6-75.8h47.7L138 320l53.5 14.5 53.7-14.5 6-62.2H84.3L71.5 112.2h241.1l-4.4 47.7z"

trame_html <- gsub("\n", "\\n", read_file("R/trame-html.html"), fixed = TRUE)

## DuckDB ----
connexion_duckdb <- dbConnect(duckdb::duckdb())
# dbExecute(connexion_duckdb, "INSTALL json;")
dbExecute(connexion_duckdb, "LOAD json;")

# https://shiny.posit.co/r/reference/shiny/latest/onstop
onStop(function() {
  dbDisconnect(connexion_duckdb, shutdown = TRUE)
})

## Dépendances via CDN ----
dependancesCDN = list(
  htmltools::htmlDependency(
    name = "jquery",
    version = "3.7.1",
    src = c(href = "https://code.jquery.com"),
    script = "jquery-3.7.1.min.js"
  ),
  htmltools::htmlDependency(
    name = "plotly-cartesian",
    version = "2.11.1",
    src = c(href = "https://cdn.plot.ly"),
    script = "plotly-cartesian-2.11.1.min.js"
  )
)
