# https://docs.posit.co/ide/user/ide/guide/tools/jobs-in-action.html
# https://github.com/sol-eng/background-jobs/tree/main/shiny-job
options(shiny.autoreload=TRUE)
shiny::runApp()
