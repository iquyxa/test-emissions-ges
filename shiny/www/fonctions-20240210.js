function telechargement(nom, data_uri) {
  var a = document.createElement('a');
  a.href = data_uri;
  a.download = nom;
  document.body.appendChild(a);
  a.click();
  URL.revokeObjectURL(data_uri);
}

function recupererLegende(nom) {
  var legende = '';
  legendes = $("div.legende[id*='" + nom.split("-")[0] + "']:visible");
  if (legendes.length == 1) {
    legende = legendes[0].innerHTML;
  }
  return legende;
}

function recupererTitre(gd, nom) {
  if (!gd.layout.hasOwnProperty('title')) {
    titres = $(":header").filter("[id*='" + nom.split("-")[0] + "']:visible");
    if (titres.length == 1) {
      gd.layout.title = {"text": "<b>" + titres[0].innerHTML + "</b>", "xref": "paper", "x": 0};
      if (gd.layout.hasOwnProperty('margin')) {
        gd.layout.margin.t = 40;
      } else {
        gd.layout.margin = {"t": 40};
      }
    }
  }
}

function exporterImage(gd, format, largeur, hauteur, nom) {
  //var legendes = document.getElementsByClassName('legende');
  var modif_annotation = false;
  var legende = recupererLegende(nom);
  
  if (legende && !gd.layout.hasOwnProperty('annotations')) {
    gd.layout.annotations = [{"text": legende, "xref": "paper", "x": 0, "xanchor": "left", "showarrow": false, "align":"left", "font": {"size": 14}}]
    modif_annotation = true;
  }
  
  recupererTitre(gd, nom);
  
  Plotly.toImage(gd, {format: 'svg', imageDataOnly: true, 'width': largeur, 'height': hauteur}).then(function(url) {
    var conteneur = document.createElement('div');
    conteneur.innerHTML = url;
    
    var svg = conteneur.getElementsByTagName('svg')[0];
    var hauteur_svg = parseInt(svg.getAttribute('height')); // = hauteur
    var viewBox = svg.getAttribute('viewBox');
    
    var elements = svg.getElementsByClassName('legendtoggle');
    while (e = elements[0]) {
      e.parentNode.removeChild(e);
    }
    
    var annotations = svg.getElementsByClassName('annotation');
    if (annotations.length > 0 && modif_annotation) {
      var annotation = annotations[0];
      var hauteur_legende = parseInt(annotation.getElementsByTagName('rect')[0].getAttribute('height'));
      var g_legende = svg.getElementsByClassName('cursor-pointer')[0];
      
      svg.setAttribute('height', hauteur_svg + hauteur_legende);
      svg.getElementsByTagName('rect')[0].setAttribute('height', hauteur + hauteur_legende);
      viewBox = viewBox.split(' ');
      viewBox[3] = hauteur_svg + hauteur_legende;
      viewBox = viewBox.join(' ');
      svg.setAttribute('viewBox', viewBox);
    
      var transform = g_legende.getAttribute('transform');
      transform = transform.split(',');
      transform = transform[0] + ',' + hauteur + ')';
      g_legende.setAttribute('transform', transform);
      
      var lien = annotation.getElementsByTagName('a');
      if (lien.length > 0) {
        lien[0].parentNode.setAttribute('style', 'font-style:italic;')
      }
      
      delete gd.layout.annotations;
    }
  
    var data_uri = 'data:image/svg+xml,' + encodeURIComponent(conteneur.innerHTML);
    
    if (format == 'png') {
      var image = new Image();
      image.width = svg.width.baseVal.value;
      image.height = svg.height.baseVal.value;
      image.src = data_uri;
      image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = image.width;
        canvas.height = image.height;
    
        var ctx = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0);
    
        telechargement(`${nom}.png`, canvas.toDataURL('image/png'));
      };
      
    } else {
      telechargement(`${nom}.svg`, data_uri);
    }
  })
}

function exporterHTML(gd, data_figure, nom, largeur, hauteur) {
  var configuration = {
      'autosizable': true,
      'displayModeBar': 'hover',
      'locale': 'fr',
      'modeBarButtons': true,
      'modeBarButtonsToAdd': ['hoverclosest', 'hovercompare'],
      'responsive': true,
      'toImageButtonOptions': {'format': 'svg', 'filename': 'graphique', 'width': largeur, 'height': hauteur}
  }
  
  var legende = recupererLegende(nom);
  recupererTitre(gd, nom);
  delete gd.layout.width;
  delete gd.layout.height;
  
  var json_plotly = {'data': gd.data, 'layout': gd.layout, 'config': configuration}
  data_figure = data_figure.replace('€', legende).replace('£', JSON.stringify(json_plotly));
  var data_uri = 'data:text/html,' + encodeURIComponent(data_figure);
  telechargement(`${nom}.html`, data_uri);
}

function toggleElement(el) {
  alert(el.style.display);
  if (el.style.display === "none") {
    el.style.display = "block";
  } else {
    el.style.display = "none";
  }
}

function redimensionnerGraphique(figure, menu) {
  if (matchMedia('only screen and (min-width: 768px)').matches) {
    var largeurTotale = $(menu).parent().innerWidth();
    var largeurMenu = $(menu).innerWidth();
    var largeurGraphique = largeurTotale - largeurMenu - 10;
    var largeurActuelle = $("div.js-plotly-plot#" + figure + " div.svg-container").innerWidth();
    if (Math.round(largeurGraphique/10)*10 != Math.round(largeurActuelle/10)*10) {
      Plotly.relayout(figure, {width: largeurGraphique});
    }
  }
}

function toggleMenuConfig(parametres, ouvrir, fermer, figure, menu, style_menu) {
  $(menu).toggleClass(style_menu);
  $(parametres).toggle();
  $(ouvrir).toggle();
  $(fermer).toggle();
  redimensionnerGraphique(figure, menu);
}

function adapterLargeurGraphiques() {
  if (document.body.scrollWidth > document.body.clientWidth) {
    menu = $("div.configuration").filter(":visible");
    if (menu.length == 1) {
      $("div.js-plotly-plot").filter(":visible").each(function() {
        redimensionnerGraphique($(this).attr("id"), "#" + menu.attr("id"));
      })
    }
  }
}
