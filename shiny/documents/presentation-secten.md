## Panorama des émissions de gaz à effet de serre en France

### Inventaire national format Secten

Mesure les émissions de gaz à effet de serre (CO<sub>2</sub>, CH<sub>4</sub>, NO<sub>2</sub>, gaz fluorés) sur le territoire national.
Format de présentation sectorielle des données utilisé par la Stratégie nationale bas-carbone.