### Pour en savoir plus

- Émissions de polluants atmosphériques et de gaz à effet de serre « Namea-Air » : [Sdes](https://www.statistiques.developpement-durable.gouv.fr/inventaire-des-emissions-de-gaz-effet-de-serre-et-des-polluants-atmospheriques-par-branches)
- Méthodologie détaillée (août 2024) : [Citepa](https://www.citepa.org/wp-content/uploads/2025/01/rapport_final_IndA_NAMEA-2024_vf.pdf) (PDF, 2 Mo, 113 pages)
- Greenhouse gas emission accounts : [Eurostat](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Greenhouse_gas_emission_accounts)

### Précisions méthodologiques

Les potentiels de réchauffement global utilisés sont ceux à 100 ans définis par le Giec dans l'AR5 de 2014 :

| Gaz                                     | PRG à 100 ans  |
| --------------------------------------- | -------------: |
| Dioxyde de carbone (CO<sub>2</sub>)     | 1              |
| Méthane (CH<sub>4</sub>)                | 28             |
| Protoxyde d'azote (N<sub>2</sub>O)      | 265            |
| Hydrofluorocarbures (HFC)               | 138 à 12 400   |
| Perfluorocarbures (PFC)                 | 6 630 à 11 100 |
| Hexafluorure de soufre (SF<sub>6</sub>) | 23 500         |
| Trifluorure d'azote (NF<sub>3</sub>)    | 16 100         |
