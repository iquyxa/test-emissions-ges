### Pour en savoir plus

- [Que représente l’empreinte carbone annuelle d’un Français ?](https://www.notre-environnement.gouv.fr/actualites/breves/article/que-represente-l-empreinte-carbone-annuelle-d-un-francais)
- [Méthodologie détaillée (novembre 2024)](https://www.statistiques.developpement-durable.gouv.fr/media/7856/download?inline) (PDF, 1,5 Mo, 58 pages)
- [Empreinte carbone : calcul par analyse en cycle de vie ou approche macro-économique ?](https://www.statistiques.developpement-durable.gouv.fr/empreinte-carbone-calcul-par-analyse-en-cycle-de-vie-ou-approche-macro-economique)
- [Dis-moi ce que tu consommes, je te donnerai ton empreinte carbone : un exercice crucial mais piégeux](https://blog.insee.fr/consommation-vs-empreinte-carbone-calcul-piegeux/)
- [Calculez votre empreinte carbone (Nos Gestes)](https://nosgestesclimat.fr/)
- [Les comptes de la Nation en 2023 - Comptes carbone](https://www.insee.fr/fr/statistiques/8194100?sommaire=8068749)
- [Greenhouse gas emission footprints (Eurostat)](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Greenhouse_gas_emission_footprints)


### Précisions méthodologiques

Le calcul de l’empreinte carbone par le Service des données et études statistiques du Ministère de la Transition écologique (SDES) couvre le CO<sub>2</sub>, le CH<sub>4</sub>, le N<sub>2</sub>O et les gaz fluorés, hors UTCATF.
Le champ géographique porte sur la France métropolitaine et les Outre-mer appartenant à l’UE (Martinique, Guadeloupe, Guyane, La Réunion, Mayotte et Saint-Martin), soit le « périmètre Kyoto ».

L’empreinte est calculée à partir d’une méthode d’analyse input/output étendue à l’environnement, promue par Eurostat et l’OCDE. Elle permet d’exprimer un niveau d’émissions en fonction de la demande finale.
L’empreinte carbone est calculée pour l’ensemble des composantes de la demande finale intérieure (consommation des ménages, des administrations publiques, des institutions sans but lucratif au service des ménages et la formation brute de capital fixe). 
