### Inventaire au format CCNUCC

L’inventaire des GES au format CCNUCC est la référence internationale.
C’est le document officiel communiqué par les États dans le cadre des engagements politiques internationaux de lutte contre le changement climatique (CCNUCC, Protocole de Kyoto).
La France rapporte tous les ans à diverses instances internationales son inventaire national de GES au format CCNUCC.
Les émissions sont réparties selon 6 sources, elles-mêmes ventilées en sous-secteurs, catégories de la nomenclature CRF.