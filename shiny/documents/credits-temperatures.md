### Pour en savoir plus

#### Température mondiale :

- [Données annuelles IGCC](https://zenodo.org/records/11388387)
- [Données mensuelles HadCRUT5](https://www.metoffice.gov.uk/hadobs/hadcrut5/)
- [Données quotidiennes ERA5 : Copernicus Climate Pulse](https://pulse.climate.copernicus.eu/)
- [Données mensuelles ERA5 : Climate bulletin Copernicus](https://climate.copernicus.eu/surface-air-temperature-maps)
- [Exploring the drivers of modern global warming](https://www.theclimatebrink.com/p/exploring-the-drivers-of-modern-global)
- [Copernicus : Tracking breaches of the 1.5⁰C global warming threshold](https://climate.copernicus.eu/tracking-breaches-150c-global-warming-threshold#54274b2e-a0d2-4a70-83e9-af47b06c2ec3)
- [Berkeley Earth : 2022 Global Temperature Anomaly Since 1850](https://berkeleyearth.org/dv/global-temperature-anomaly-from-1850-2022/)
- [NOAA : Global Temperature](https://www.climate.gov/news-features/understanding-climate/climate-change-global-temperature)
- [Met Office - Indicators of Global Warming](https://climate.metoffice.cloud/current_warming.html)
- [Climate Reanalyzer : Daily 2-meter Air Temperature](https://climatereanalyzer.org/clim/t2_daily/)
- [Données quotidiennes JRA-55](https://climatlas.com/temperature/jra55_temperature.php)
- [Prévisions NCEP GFS](http://karstenhaustein.com/reanalysis/gfs0p5/GFS_anomaly_timeseries_global.html)
- [Requêter directement les données avec DuckDB Wasm](https://shell.duckdb.org/#queries=v0,create-table-era5-as-select-*-from-'https%3A%2F%2Fgth.ovh%2Fparquet%2Ftemperatures%2Ftemp_quot_era5.parquet'~,describe-era5~,select-min(date)%2C-max(date)-%2C-min(anomalie)-%2C-argmin(date%2C-anomalie)-%2C-max(anomalie)%2C-argmax(date%2C-anomalie)-from-era5~,select-*-from-era5-order-by-date-desc-limit-10~,from-era5-select-*-order-by-anomalie-desc-limit-10~,from-era5-select-date_trunc('month'%2C-date)-as-mois%2C-round(avg(anomalie)%2C-3)-as-anomalie%2C-count(*)-as-nb_jours-where-mois-%3C-date_trunc('month'%2C-today())-group-by-mois-order-by-anomalie-desc-limit-10~,with-temp_an-as-(select-year(date)-as-annee%2C-avg(anomalie)-as-anomalie%2C-from-era5-where-annee-%3C-year(today())-group-by-annee)%2Cglissement-as-(select-annee%2C-round(avg(anomalie)-OVER-a10%2C-2)-as-moyenne%2C-first(annee)-over-a10-||-'|'-||-last(annee)-over-a10-as-periode-from-temp_an-WINDOW-a10-as-(ORDER-BY-annee-rows-between-9-PRECEDING-and-0-FOLLOWING))-from-glissement-select-periode%2C-moyenne-order-by-annee-desc-limit-10~)

En 2024 :
- [Copernicus - Global Climate Highlights 2024](https://climate.copernicus.eu/global-climate-highlights-2024)
- [State of the climate: 2024 sets a new record as the first year above 1.5C](https://www.carbonbrief.org/state-of-the-climate-2024-sets-a-new-record-as-the-first-year-above-1-5c/)
- [Berkeley Earth - Global Temperature Report for 2024](https://berkeleyearth.org/global-temperature-report-for-2024/)

#### Température en France métropolitaine :

- [Données Ribes et al.](https://zenodo.org/record/7002816)
- [Code R Ribes et al.](https://gitlab.com/ribesaurelien/france_study/)
- [Météo-France - Données climatologiques de base - quotidiennes](https://meteo.data.gouv.fr/datasets/6569b51ae64326786e4e8e1a)
- [Le Monde - Visualisez le réchauffement climatique en France et dans votre ville ](https://www.lemonde.fr/les-decodeurs/article/2021/01/06/visualisez-le-rechauffement-climatique-en-france-et-dans-votre-ville-avec-nos-barres-de-rechauffement_6065388_4355770.html)
- [Onerc - Impacts du changement climatique](https://www.ecologie.gouv.fr/politiques-publiques/impacts-du-changement-climatique-atmosphere-temperatures-precipitations)
