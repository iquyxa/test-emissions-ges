### Pour en savoir plus

- Estimation Secten des émissions 2023 : [Citepa](https://www.citepa.org/emissions-de-gaz-a-effet-de-serre-en-france-nouvelle-estimation-lannee-2023-avec-les-donnees-secten-du-citepa/)
- Méthodologie détaillée (juillet 2024) : [Citepa - rapport Ominea](https://www.citepa.org/donnees-air-climat/methodologie-de-linventaire-ominea/)
- Baromètre mensuel des émissions : [Citepa](https://www.citepa.org/donnees-air-climat/donnees-gaz-a-effet-de-serre/barometre-des-emissions-mensuelles/)
- Stratégie Nationale Bas-Carbone : [Ministère de la Transition écologique](https://www.ecologie.gouv.fr/politiques-publiques/strategie-nationale-bas-carbone-snbc)
- La biomasse énergie est-elle neutre en carbone ? ([Citepa](https://www.infothek-biomasse.ch/images/424_2020_Mathias_Biomasse_%C3%A9nergie_et_neutralit%C3%A9_carbone.pdf))

### Précisions méthodologiques

Les potentiels de réchauffement global utilisés sont ceux à 100 ans définis par le Giec dans l'AR5 de 2014 :

| Gaz                                     | PRG à 100 ans  |
| --------------------------------------- | -------------: |
| Dioxyde de carbone (CO<sub>2</sub>)     | 1              |
| Méthane (CH<sub>4</sub>)                | 28             |
| Protoxyde d'azote (N<sub>2</sub>O)      | 265            |
| Hydrofluorocarbures (HFC)               | 138 à 12 400   |
| Perfluorocarbures (PFC)                 | 6 630 à 11 100 |
| Hexafluorure de soufre (SF<sub>6</sub>) | 23 500         |
| Trifluorure d'azote (NF<sub>3</sub>)    | 16 100         |
