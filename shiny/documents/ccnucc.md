### Pour en savoir plus

- Rapport détaillé de l'inventaire CCNUCC édition 2024 : [Rapport National d’Inventaire pour la France au titre de la Convention cadre des Nations Unies sur les Changements Climatiques](https://www.citepa.org/wp-content/uploads/2025/01/CCNUCC_france_dec2024.pdf) (PDF, 19 Mo, 968 pages)
- Données détaillées par pays : [Greenhouse Gas Inventory Data - Detailed data by Party](https://di.unfccc.int/detailed_data_by_party)
- Objectifs européens : [La Commission Européenne recommande une réduction nette des émissions de gaz à effet de serre de 90 % d'ici à 2040 par rapport aux niveaux de 1990](https://france.representation.ec.europa.eu/informations/la-commission-presente-une-recommandation-pour-un-objectif-de-reduction-des-emissions-lhorizon-2040-2024-02-06_fr)

### Précisions méthodologiques

Les potentiels de réchauffement global utilisés sont ceux à 100 ans définis par le Giec dans l'AR5 de 2014 :

| Gaz                                     | PRG à 100 ans  |
| --------------------------------------- | -------------: |
| Dioxyde de carbone (CO<sub>2</sub>)     | 1              |
| Méthane (CH<sub>4</sub>)                | 28             |
| Protoxyde d'azote (N<sub>2</sub>O)      | 265            |
| Hydrofluorocarbures (HFC)               | 138 à 12 400   |
| Perfluorocarbures (PFC)                 | 6 630 à 11 100 |
| Hexafluorure de soufre (SF<sub>6</sub>) | 23 500         |
| Trifluorure d'azote (NF<sub>3</sub>)    | 16 100         |
