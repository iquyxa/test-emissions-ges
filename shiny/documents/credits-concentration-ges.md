*Sources : [Historical greenhouse gas concentrations for climate modelling (CMIP6), Geosci](https://gmd.copernicus.org/articles/10/2057/2017/gmd-10-2057-2017.html) pour la période historique ; [NOAA](https://gml.noaa.gov/ccgg/trends/gl_data.html) pour la période récente*

Pour en savoir plus :
- [Copernicus - Greenhouse gas fluxes](https://climate.copernicus.eu/climate-indicators/greenhouse-gas-fluxes)
- [Met Office - Mauna Loa carbon dioxide forecast for 2025](https://www.metoffice.gov.uk/research/climate/seasonal-to-decadal/long-range/forecasts/co2-forecast-for-2025)
- [Zack Labe - Climate change indicators](https://zacklabe.com/climate-change-indicators/)
- [How achievable is the Methane Pledge?](https://www.nilu.com/2022/04/how-achievable-is-the-methane-pledge/)