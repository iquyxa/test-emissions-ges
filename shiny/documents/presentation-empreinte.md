### Empreinte carbone

Mesure les émissions de gaz à effet de serre induites par la consommation des Français.
Prend en compte les émissions importées, et exclut les émissions dues à la production française exportée.