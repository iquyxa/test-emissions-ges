# TODO

## Régression linéaire
- sur les n dernières années
- ajouter radioButton
- légende : +0,2°C/décennie + R²
- https://www.carbonbrief.org/factcheck-why-the-recent-acceleration-in-global-warming-is-what-scientists-expect/#iframeMob
- https://stackoverflow.com/questions/15180008/how-to-calculate-the-95-confidence-interval-for-the-slope-in-a-linear-regressio

## DuckDB
/home/guilhem/developpement/R/shiny/emissions-ges/preparation/era5.sh

## Empreinte carbone
- Màj données par gaz depuis 1990 : https://www.statistiques.developpement-durable.gouv.fr/emissions-de-gaz-effet-de-serre-et-empreinte-carbone-en-2023

## Graphique
- Accès direct "http://127.0.0.1:3491/#temperature?periodicite=J&ga=1&variable=Anomalie" : req(!FIRST, cancelOutput = TRUE) ?
    - priority ?
    -> ui selected ?
- glissement annuel : mettre en cache uniquement histogramme + restyle GA ?

```
plotlyProxy("temperatures", session) %>%
  plotlyProxyInvoke("restyle", list(
    x = list(temp_glissement$date),
    y = list(temp_glissement$glissement),
    name = glue("Glissement sur {ga} ans"),
    text = list(temp_glissement$periode_glissement),
    hovertemplate = "%{text} : %{y:>+.2f} °C<extra></extra>"
    ), list(1))
```

## Liens
- infographie sur le réchauffement climatique : https://www.lemonde.fr/les-decodeurs/visuel/2023/06/07/comprendre-le-rechauffement-comment-nous-avons-bouleverse-le-climat_6176490_4355770.html
- [How much have temperatures risen in countries across the world?](https://ourworldindata.org/temperature-anomaly)

## Divers
- treemap : faire manuellement le SVG + info-bulles ?
- Revoir API (supprimer cache)
- No warming since...
- Bug si accès direct graphique : manque export perso SVG/html...
    https://emissions.gth.ovh/#temperature?periodicite=J&ga=1&variable=Anomalie
- Graphiques en niveau : proposer version non empilée

